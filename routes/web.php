<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//primera ruta de get 
Route::get('hola',function(){
    echo"hola estoy en laravel";
});
Route::get('arreglo',function(){
    //crear un areglo de estudaiantes
    $estudiantes = [  "AD"=>"andres",
    "LA" =>"laura",
    "ANN" =>"ana" ,
    "RO"=> "rodrigo"
    ];
    //var_dump($estudiantes);

    //recorrer un areglo 
    foreach($estudiantes as $key => $value ){
        echo"$value tiene indice $key <hr/>";
    } 
});

Route::get('paises',function(){
    //crear un areglocon informacion de paises
    $paises = [
        "Colombia" =>[
                "capital"=>"Bogota",
                "moneda"=>"peso",
                "poblacion" => 50.372
        ],
        "Ecuador"=>[
                 "capital"=>"Quito",
                 "moneda"=>"Dolar",
                "poblacion" => 17.517
        ],
        "Brazil"=>[
                "capital"=>"Brazilia",
                "moneda"=>"real",
                "poblacion" => 212.216
        ],
        "Bolivia"=>[
                "capital"=>"La paz",
                "moneda"=>"Boliviano",
                "poblacion" => 13.633
            ]
           
            
    ];

    return view('Paises')->with ("paises" , $paises);












    //recorrer una dimension del arreglo


    //foreach ($paises as $pais => $infopais){
        //echo "<h2> $pais </h2>";
        //echo "capital:" .$infopais["capital"] ."<br/>";
        //echo "moneda:" .$infopais["moneda"] ."<br/>";
        //echo "poblacion (en millones de habitantes):" .$infopais["poblacion"] ."<br/>";
        //echo "<hr/>";
    //}


    //mostrar una  vistas para mostrar paises
    //en mvc yo puedo pasar datos a una vistas
    



       
    });

